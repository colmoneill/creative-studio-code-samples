
function currentTime() {
    var date = new Date(); /* creating object of Date class */
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    var operand = 4;
    document.getElementById("main").innerHTML = hour + " : " + min + " : " + sec;
    var visHours = document.getElementById("hours");
    visHours.style.width = hour * operand + "px";
    var visMins = document.getElementById("minutes");
    visMins.style.width = min * operand + "px";
    var visSecs = document.getElementById("seconds");
    visSecs.style.width = sec * (operand * 2) +"px";
    if (sec < 10) {
        visHours.style.opacity = "0.0" + sec;
        visMins.style.opacity = "0.0" + sec;
        visSecs.style.opacity = "0.0" + sec;
    }
    else{
        visHours.style.opacity = "0." + sec;
        visMins.style.opacity = "0." + sec;
        visSecs.style.opacity = "0." + sec;
    }
    var t = setTimeout(function(){ currentTime() }, 1000); /* setting timer */
}

currentTime()
