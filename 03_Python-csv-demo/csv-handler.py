import csv

with open('adam-spotify-data.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')    
    
    for row in csv_reader:
        artist_track_str = "Artist: " + row[3] + "/ Track: " + row[1] + '\n';
        # prints formatting to console
        print(artist_track_str)
        print()    
        # creates a new file and inserts the formatted content
        f = open('output-after-python.txt', 'a')
        f.write(artist_track_str)
        f.close()
