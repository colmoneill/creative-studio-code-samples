import os
from exif import Image
path = "/home/colm/OneDrive/2022_23/3_Creative-Studio/creative-studio-code-samples/04_Python-photo-exif-example/photos/"
for file in os.listdir(path):
    img_path = path + file
    # print(img_path)
    with open(img_path, 'rb') as image_file:
        my_image = Image(image_file)
        print(my_image.datetime)
        f = open('output-after-python.txt', 'a')
        f.write(my_image.datetime)
        f.close()
