# pip install exif
# https://pypi.org/project/exif/

from exif import Image

with open('photos/IMG_20221115_094358.jpg', 'rb') as image_file:
    my_image = Image(image_file)
    print(my_image.list_all())
    print()
    print(my_image.datetime)
    print(my_image.image_width)
    print(my_image.image_height)
    print(my_image.make)
    print(my_image.model)
    print(my_image.orientation)